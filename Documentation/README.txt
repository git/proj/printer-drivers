This overlay contains user-submitted ebuilds for printer drivers.

All Gentoo developers have push access and may do as they please.

Push access may also be granted to users who want to maintain a particular 
package here. The details are still to be decided.


Packages in this overlay have not received the same amount of testing
and checking as packages in the main tree. If you use them, you may encounter 
problems. You have been warned.

If you report bugs on Gentoo bugzilla, please prepend the summary 
with "[printer-drivers overlay]", so it's immediately clear where the 
ebuild is coming from.

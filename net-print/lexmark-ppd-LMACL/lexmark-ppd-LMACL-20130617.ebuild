# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

MODEL_CODE=${PN//lexmark-ppd-}
MODEL_NAMES="T650, T652, T654, T656, TG654"
inherit cups-lexmark

SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""
